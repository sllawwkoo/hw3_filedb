import FileDB from "./FileDB.js";

(async () => {
	// Створення екземпляру бази даних та реєстрація схеми
	const fileDB = new FileDB("data.json");
	const newspostSchema = {
		id: Number,
		title: String,
		text: String,
		createDate: Date,
	};
	await fileDB.registerSchema("newspost", newspostSchema);
	// Отримання таблиці та виконання CRUD операцій
	const newspostTable = fileDB.getTable("newspost");

	// Отримання схеми для таблиці "newspost"
	const schemaNewspost = newspostTable.getSchema();
	console.log("Schema for newspost table:", schemaNewspost);

	//Додавання нового запису
	const createdNewspost = await newspostTable.create({
		title: "У зоопарку Чернігова лисичка народила лисеня",
		text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
	});
	console.log("Created newspost:", createdNewspost);

	//Оновлення запису за вказаним id
	const updatedNewspost = await newspostTable.update(createdNewspost.id, {
		title: "Маленька лисичка",
	});
	console.log("Updated newspost:", updatedNewspost);

	// Отримання запису за його ідентифікатором
	const postId = createdNewspost.id; // Припустимо, що це id запису, який ми шукаємо
	const newspostById = await newspostTable.getById(postId);

	if (newspostById) {
		console.log("Found newspost by id:", newspostById);
	} else {
		console.log(`Newspost with id ${postId} not found.`);
	}

	//Додавання нового запису з полем якого немає в схемі
	const createdSecondpost = await newspostTable.create({
		title: "У зоопарку Київа тигриця народила тигреня",
		text: "В Київському зоопарку відбулося диво! Тигриця на ім'я Маха народила маленьке тигреня! Приходьте швидше, щоб побачити цю чудову подію!",
		additionalInfo:
			"За даними зоопарку, тигреня здорове та активне, і мати доглядає за ним дуже уважно.",
	});
	console.log("Created Second newspost:", createdSecondpost);

	//Оновлення запису за вказаним id з полем якого немає в схемі
	const updatedSecondpost = await newspostTable.update(createdSecondpost.id, {
		title: "Велика тигриця",
		author: "Іванова Марія",
	});
	console.log("Updated Second newspost:", updatedSecondpost);

	//Повернення усіх записів у базі у вигляді масиву
	const allNewsposts = await newspostTable.getAll();
	console.log("All newsposts:", allNewsposts);

	// Видалення запису за вказаним id та повернення id видаленого запису
	const deletedId = await newspostTable.delete(createdNewspost.id);
	console.log("Deleted newspost id:", deletedId);

	//Повернення усіх записів у базі у вигляді масиву
	const allNewsposts2 = await newspostTable.getAll();
	console.log("All newsposts:", allNewsposts2);
})();
