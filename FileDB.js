import fsPromise from "fs/promises";

class FileDB {
	constructor() {
		this.schema = {};
	}

	//Метод для реєстрації схеми для таблиці.
	async registerSchema(tableName, schema) {
		try {
			this.schema = { [tableName]: schema };
			await fsPromise.writeFile(
				`${tableName}.json`,
				JSON.stringify([], null, 2)
			);
		} catch (error) {
			console.log("Failed to save data:", error);
		}
	}

	//Метод для завантаження даних з файлу.
	async readFile(tableName) {
		try {
			const data = await fsPromise.readFile(`${tableName}.json`, "utf8");
			return JSON.parse(data);
		} catch (error) {
			console.log("File does not exist or is empty.");
		}
	}

	//Метод для збереження поточних даних у файл.
	async writeFile(tableName, table) {
		try {
			await fsPromise.writeFile(
				`${tableName}.json`,
				JSON.stringify(table, null, 2)
			);
		} catch (error) {
			console.log("Failed to save data:", error);
		}
	}

	//Метод для отримання об'єкта, який представляє таблицю з бази даних. Він має наступні методи:
	getTable(tableName) {
		return {
			//Повертає схему таблиці.
			getSchema: () => {
				return this.schema[tableName];
			},

			//Повертає всі записи у вигляді масиву.
			getAll: async () => {
				const table = await this.readFile(tableName);
				return table;
			},

			//Повертає запис з вказаним id.
			getById: async (id) => {
				const table = await this.readFile(tableName);
				return table.find((item) => item.id === id);
			},

			//Додає новий запис до таблиці за допомогою переданих даних.
			create: async (data) => {
				const { title, text } = data;

				const newItem = {
					id: Number(
						`${Math.floor(Date.now() / 1000)}${
							Math.floor(Math.random() * 100) + 1
						}`
					),
					createDate: new Date().toLocaleDateString(),
					title,
					text,
					...this.validate(tableName, data),
				};
				const table = await this.readFile(tableName);
				table.push(newItem);
				await this.writeFile(tableName, table);
				return newItem;
			},

			//Оновлює запис з вказаним id за допомогою нових даних.
			update: async (id, newData) => {
				const table = await this.readFile(tableName);
				const index = table.findIndex((record) => record.id === id);
				if (index !== -1) {
					const updatedItem = {
						...table[index],
						...this.validate(tableName, newData, table[index]),
					};
					table[index] = updatedItem;
					await this.writeFile(tableName, table);
					return updatedItem;
				} else {
					throw new Error(`Item with id ${id} not found.`);
				}
			},

			//Видаляє запис з вказаним id.
			delete: async (id) => {
				const table = await this.readFile(tableName);
				const index = table.findIndex((item) => item.id === id);
				if (index !== -1) {
					const deletedId = table[index].id;
					table.splice(index, 1);
					await this.writeFile(tableName, table);
					return deletedId;
				} else {
					throw new Error(`Item with id ${id} not found.`);
				}
			},
		};
	}

	//Внутрішній метод для перевірки ключів об"єкту при створенні нового запису чи при оновленні існуючого з ключами схеми.
	validate(tableName, data, currentItem = null) {
		const schema = this.getTable(tableName).getSchema();

		if (!schema) {
			console.log(`Schema not found for table '${tableName}'`);
			return null;
		}

		const item = currentItem ? { ...currentItem } : {};
		const validatedItem = {};

		for (const key in data) {
			if (!(key in schema)) {
				console.log(`Key '${key}' not found in Schema`);
				continue;
			}
			item[key] = data[key];
		}

		if (item.title && item.title.trim() !== "") {
			validatedItem.title = item.title;
		}

		if (item.text && item.text.trim() !== "") {
			validatedItem.text = item.text;
		}

		if (Object.keys(validatedItem).length > 0) {
			console.log(`validatedItem: ${JSON.stringify(validatedItem, null, 2)}`);
			return validatedItem;
		} else {
			throw new Error("Item is not valid");
		}
	}
}

export default FileDB;
